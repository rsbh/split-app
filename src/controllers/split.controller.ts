import { MinHeap, MaxHeap, IGetCompareValue } from "@datastructures-js/heap";
import { Contribution } from "../models/contribution";

interface SharesMap {
  [userId: number]: number;
}

export type BalanceMap = SharesMap;

export interface Balance {
  userId: number;
  amount: number;
}

const compareBalance: IGetCompareValue<Balance> = (bal: Balance) => bal.amount;

export default class SplitController {
  public static getEqualShares(contributions: Contribution[]): SharesMap {
    const total = contributions.reduce((acc, cur) => acc + cur.amount, 0);
    const usersLength = contributions.length;
    const shares = contributions.reduce((acc, contribution) => {
      const share = total / usersLength;
      acc[contribution.userId] = share;
      return acc;
    }, {} as SharesMap);
    return shares;
  }

  public static getBalances(contributions: Contribution[]): BalanceMap {
    const shareMap = SplitController.getEqualShares(contributions);
    const balances = contributions.reduce((acc, contribution) => {
      const share = shareMap[contribution.userId];
      acc[contribution.userId] = contribution.amount - share;
      return acc;
    }, {} as BalanceMap);
    return balances;
  }

  public static mergeBalances(balanceMap: BalanceMap[]): BalanceMap {
    const mergedBalances = balanceMap.reduce((acc, bal) => {
      Object.keys(bal).forEach((userId) => {
        const uId = Number(userId);
        const prevBal = acc[uId] || 0;
        acc[uId] = bal[uId] + prevBal;
      });
      return acc;
    }, {} as BalanceMap);

    return mergedBalances;
  }

  public static getBalanceList(balanceMap: BalanceMap): Balance[] {
    return Object.keys(balanceMap).map((userId) => {
      const uId = Number(userId);
      const amount = balanceMap[uId];
      return { userId: uId, amount };
    });
  }

  public static simplifyBalances(balanceMap: BalanceMap): BalanceMap {
    const positiveBalances = new MaxHeap<Balance>(compareBalance);
    const newgativeBalances = new MinHeap<Balance>(compareBalance);
    Object.keys(balanceMap).forEach((userId) => {
      const uId = Number(userId);
      const amount = balanceMap[uId];
      const bal = { userId: uId, amount };
      if (amount > 0) {
        positiveBalances.insert(bal);
      } else {
        newgativeBalances.insert(bal);
      }
    });

    while (positiveBalances.size() > 0 && newgativeBalances.size() > 0) {
      const maxPositive = positiveBalances.extractRoot();
      const maxNegative = newgativeBalances.extractRoot();

      const positiveBal = maxPositive.amount;
      const negativeBal = maxNegative.amount * -1;

      const diff = positiveBal - negativeBal;
      balanceMap[maxPositive.userId] = diff;

      if (diff > 0) {
        positiveBalances.insert({
          userId: maxPositive.userId,
          amount: diff,
        });
      } else if (diff < 0) {
        newgativeBalances.insert({
          userId: maxNegative.userId,
          amount: diff * -1,
        });
      }
    }
    return balanceMap;
  }
}
