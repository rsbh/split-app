/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  generateGroupData,
  generateGroupPayload,
  generateGroupsData,
} from "../../test/utils/generate";
import GroupController from "./group.controller";

afterEach(() => {
  jest.resetAllMocks();
});

describe("GroupController", () => {
  describe("getGroups", () => {
    it("should return empty array", async () => {
      const repo = {
        group: {
          find: jest.fn().mockReturnValue([]),
        },
      } as any;

      const spy = jest.spyOn(repo.group, "find");

      const controller = new GroupController(repo);
      const groups = await controller.getGroups();
      expect(groups).toEqual([]);
      expect(spy).toHaveBeenCalledWith();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it("should return an array of Groups", async () => {
      const GroupsData = generateGroupsData(2);

      const repo = {
        group: {
          find: jest.fn().mockReturnValue(GroupsData),
        },
      } as any;

      const spy = jest.spyOn(repo.group, "find");
      const controller = new GroupController(repo);
      const groups = await controller.getGroups();
      expect(groups).toEqual(GroupsData);
      expect(spy).toHaveBeenCalledWith();
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe("createGroup", () => {
    it("should add Group to the database", async () => {
      const payload = generateGroupPayload();
      const groupData = generateGroupData(payload);

      const repo = {
        group: {
          save: jest.fn().mockReturnValue(groupData),
        },
      } as any;

      const spy = jest.spyOn(repo.group, "save");

      const controller = new GroupController(repo);
      const Group = await controller.createGroup(payload);
      expect(Group).toMatchObject(payload);
      expect(Group).toEqual(groupData);
      expect(spy).toHaveBeenCalledWith(payload);
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe("getGroupById", () => {
    test("should return Group from the database", async () => {
      const id = 1;
      const groupData = generateGroupData({ id });

      const repo = {
        group: {
          findOneBy: jest.fn().mockReturnValue(groupData),
        },
      } as any;

      const spy = jest.spyOn(repo.group, "findOneBy");

      const controller = new GroupController(repo);
      const group = await controller.getGroupById(id);
      expect(group).toEqual(groupData);
      expect(group?.id).toBe(id);
      expect(spy).toHaveBeenCalledWith({ id });
      expect(spy).toHaveBeenCalledTimes(1);
    });

    test("should return null if Group not found", async () => {
      const id = 1;

      const repo = {
        group: {
          findOneBy: jest.fn().mockReturnValue(null),
        },
      } as any;

      const spy = jest.spyOn(repo.group, "findOneBy");

      const controller = new GroupController(repo);
      const group = await controller.getGroupById(id);
      expect(group).toBeNull();
      expect(spy).toHaveBeenCalledWith({ id });
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
