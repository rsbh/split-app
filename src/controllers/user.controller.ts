import { AppRepositories } from "../repositories";

interface CreateUserPayload {
  name: string;
  email: string;
  password: string;
}

export default class UserController {
  repos: AppRepositories;

  constructor(repos: AppRepositories) {
    this.repos = repos;
  }
  async getUsers() {
    const users = await this.repos.user.find();
    return users;
  }

  async createUser(payload: CreateUserPayload) {
    const user = await this.repos.user.save(payload);
    return user;
  }

  async getUserById(id: number) {
    const user = await this.repos.user.findOneBy({
      id: Number(id),
    });
    return user;
  }
}
