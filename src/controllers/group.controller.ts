import { In } from "typeorm";
import { AppRepositories } from "../repositories";

interface CreateGroupPayload {
  name: string;
}

export default class GroupController {
  repos: AppRepositories;

  constructor(repos: AppRepositories) {
    this.repos = repos;
  }

  async getGroups() {
    const groups = await this.repos.group.find();
    return groups;
  }

  async createGroup(payload: CreateGroupPayload) {
    const group = await this.repos.group.save(payload);
    return group;
  }

  async getGroupById(id: number) {
    const group = await this.repos.group.findOneBy({
      id: Number(id),
    });
    return group;
  }

  async getGroupMembers(groupId: number) {
    const groups = await this.repos.user
      .createQueryBuilder("user")
      .leftJoinAndSelect("user.groups", "group")
      .where("group.id = :id", { id: groupId })
      .getMany();
    return groups;
  }

  async addGroupMembers(groupId: number, userIds: number[]) {
    const [users, group] = await Promise.all([
      this.repos.user.findBy({
        id: In(userIds),
      }),
      this.repos.group.findOne({
        where: {
          id: groupId,
        },
        relations: ["members"],
      }),
    ]);
    if (group && users.length > 0) {
      group.members = [...group.members, ...users];
      await this.repos.group.save(group);
      return group;
    } else {
      return new Error("Group or users not found");
    }
  }
}
