/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  generateUserData,
  generateUserPayload,
  generateUsersData,
} from "../../test/utils/generate";
import UserController from "./user.controller";

afterEach(() => {
  jest.resetAllMocks();
});

describe("UserController", () => {
  describe("getUsers", () => {
    it("should return empty array", async () => {
      const repo = {
        user: {
          find: jest.fn().mockReturnValue([]),
        },
      } as any;

      const spy = jest.spyOn(repo.user, "find");

      const controller = new UserController(repo);
      const users = await controller.getUsers();
      expect(users).toEqual([]);
      expect(spy).toHaveBeenCalledWith();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it("should return an array of users", async () => {
      const usersData = generateUsersData(2);

      const repo = {
        user: {
          find: jest.fn().mockReturnValue(usersData),
        },
      } as any;

      const spy = jest.spyOn(repo.user, "find");
      const controller = new UserController(repo);
      const users = await controller.getUsers();
      expect(users).toEqual(usersData);
      expect(spy).toHaveBeenCalledWith();
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe("createUser", () => {
    it("should add user to the database", async () => {
      const payload = generateUserPayload();
      const userData = generateUserData(payload);

      const repo = {
        user: {
          save: jest.fn().mockReturnValue(userData),
        },
      } as any;

      const spy = jest.spyOn(repo.user, "save");

      const controller = new UserController(repo);
      const user = await controller.createUser(payload);
      expect(user).toMatchObject(payload);
      expect(user).toEqual(userData);
      expect(spy).toHaveBeenCalledWith(payload);
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe("getUserById", () => {
    test("should return user from the database", async () => {
      const id = 1;
      const userData = generateUserData({ id });

      const repo = {
        user: {
          findOneBy: jest.fn().mockReturnValue(userData),
        },
      } as any;

      const spy = jest.spyOn(repo.user, "findOneBy");

      const controller = new UserController(repo);
      const user = await controller.getUserById(id);
      expect(user).toEqual(userData);
      expect(user?.id).toBe(id);
      expect(spy).toHaveBeenCalledWith({ id });
      expect(spy).toHaveBeenCalledTimes(1);
    });

    test("should return null if user not found", async () => {
      const id = 1;

      const repo = {
        user: {
          findOneBy: jest.fn().mockReturnValue(null),
        },
      } as any;

      const spy = jest.spyOn(repo.user, "findOneBy");

      const controller = new UserController(repo);
      const user = await controller.getUserById(id);
      expect(user).toBeNull();
      expect(spy).toHaveBeenCalledWith({ id });
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
