import { Contribution } from "../models/contribution";
import SplitController, { BalanceMap } from "./split.controller";

describe("SplitController", () => {
  describe("getEqualShares", () => {
    it("return empty map for empty contributions", () => {
      const contributions: Contribution[] = [];
      const shares = SplitController.getEqualShares(contributions);
      expect(shares).toEqual({});
    });

    it("return equal divide the shares", () => {
      const contributions = [
        { amount: 200, userId: 1 },
        { amount: 250, userId: 2 },
        { amount: 0, userId: 3 },
      ] as Contribution[];
      const shares = SplitController.getEqualShares(contributions);
      expect(shares).toEqual({
        1: 150,
        2: 150,
        3: 150,
      });
    });
  });

  describe("getBalances", () => {
    it("return empty map for empty contributions", () => {
      const contributions: Contribution[] = [];
      const shares = SplitController.getBalances(contributions);
      expect(shares).toEqual({});
    });

    it("return equal divide the shares", () => {
      const contributions = [
        { amount: 200, userId: 1 },
        { amount: 250, userId: 2 },
        { amount: 0, userId: 3 },
      ] as Contribution[];
      const shares = SplitController.getBalances(contributions);
      expect(shares).toEqual({
        1: 50,
        2: 100,
        3: -150,
      });
    });
  });

  describe("mergeBalances", () => {
    it("return empty map for empty contributions", () => {
      const balances: BalanceMap[] = [];
      const mergeMap = SplitController.mergeBalances(balances);
      expect(mergeMap).toEqual({});
    });

    it("should add user balances", () => {
      const balances: BalanceMap[] = [
        { 1: 100, 2: 200 },
        { 1: -100, 2: 200 },
      ];
      const mergeMap = SplitController.mergeBalances(balances);
      expect(mergeMap).toEqual({
        1: 0,
        2: 400,
      });
    });

    it("should combine all user", () => {
      const balances: BalanceMap[] = [
        { 1: 100, 2: 200 },
        { 3: -100, 2: 200 },
        { 3: 100, 4: 500 },
      ];
      const mergeMap = SplitController.mergeBalances(balances);
      expect(mergeMap).toEqual({
        1: 100,
        2: 400,
        3: 0,
        4: 500,
      });
    });
  });

  describe("getBalanceList", () => {
    it("return empty list for empty balances", () => {
      const balances: BalanceMap = {};
      const balanceList = SplitController.getBalanceList(balances);
      expect(balanceList).toEqual([]);
    });

    it("return list of user balances", () => {
      const balances: BalanceMap = { 1: 250, 2: 100, 3: -150 };
      const balanceList = SplitController.getBalanceList(balances);
      expect(balanceList).toEqual([
        { userId: 1, amount: 250 },
        { userId: 2, amount: 100 },
        { userId: 3, amount: -150 },
      ]);
    });
  });

  describe("simplifyBalances", () => {
    it("should settle user balances", () => {
      const balances: BalanceMap = { 1: 250, 2: 100, 3: -150 };
      const balanceList = SplitController.simplifyBalances(balances);
      expect(balanceList).toEqual({ "1": 100, "2": 100, "3": 0 });
    });
  });
});
