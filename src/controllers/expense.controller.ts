import { AppRepositories } from "../repositories";
import SplitController from "./split.controller";

interface CreateExpensePayload {
  name: string;
  amount: number;
  groupId?: number;
  contributions?: {
    userId: number;
    amount: number;
  }[];
}

export default class ExpenseController {
  repos: AppRepositories;

  constructor(repos: AppRepositories) {
    this.repos = repos;
  }

  async getExpenses() {
    const expenses = await this.repos.expense.find();
    return expenses;
  }

  async createExpense(payload: CreateExpensePayload) {
    const expense = await this.repos.expense.save(payload);
    return expense;
  }

  async getExpenseById(expenseId: number) {
    const expenses = await this.repos.expense.findOne({
      where: { id: expenseId },
    });
    return expenses;
  }

  async getGroupExpenses(groupId: number) {
    const expenses = await this.repos.expense.find({
      where: { groupId },
    });
    return expenses;
  }

  async getGroupSummary(groupId: number) {
    const expenses = await this.getGroupExpenses(groupId);
    const balances = expenses.map((e) =>
      SplitController.getBalances(e.contributions)
    );
    return SplitController.mergeBalances(balances);
  }
}
