import express from "express";
import ExpenseController from "../controllers/expense.controller";
import { getAppRepositories } from "../repositories";

const router = express.Router();

router.get("/", async (req, res) => {
  const controller = new ExpenseController(getAppRepositories());
  const expenses = await controller.getExpenses();
  res.json({ expenses });
});

router.post("/", async (req, res) => {
  try {
    const controller = new ExpenseController(getAppRepositories());
    await controller.createExpense(req.body);
    res.status(201).send();
  } catch (error) {
    console.error(error);
    res.status(500).send();
  }
});

router.get("/:id", async (req, res) => {
  const id = Number(req.params.id);
  if (id && !isNaN(id)) {
    const controller = new ExpenseController(getAppRepositories());
    const expense = await controller.getExpenseById(id);
    res.json({ expense });
  } else {
    res.status(400).send();
  }
});

router.get("/groups/:id", async (req, res) => {
  const groupId = Number(req.params.id);
  if (groupId && !isNaN(groupId)) {
    const controller = new ExpenseController(getAppRepositories());
    const expense = await controller.getGroupExpenses(groupId);
    res.json({ expense });
  } else {
    res.status(400).send();
  }
});

router.get("/groups/:id/summary", async (req, res) => {
  const groupId = Number(req.params.id);
  if (groupId && !isNaN(groupId)) {
    const controller = new ExpenseController(getAppRepositories());
    const summary = await controller.getGroupSummary(groupId);
    res.json({ summary });
  } else {
    res.status(400).send();
  }
});

export default router;
