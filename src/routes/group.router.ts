import express from "express";
import GroupController from "../controllers/group.controller";

import { getAppRepositories } from "../repositories";
const router = express.Router();

router.get("/", async (req, res) => {
  const controller = new GroupController(getAppRepositories());
  const groups = await controller.getGroups();
  res.json({ groups });
});

router.post("/", async (req, res) => {
  const controller = new GroupController(getAppRepositories());
  await controller.createGroup(req.body);
  res.status(201).send();
});

router.get("/:id", async (req, res) => {
  const id = Number(req.params.id);
  if (id && !isNaN(id)) {
    const controller = new GroupController(getAppRepositories());
    const group = await controller.getGroupById(id);
    res.json({ group });
  } else {
    res.status(400).send();
  }
});

router.get("/:id/members", async (req, res) => {
  const id = Number(req.params.id);
  if (id && !isNaN(id)) {
    const controller = new GroupController(getAppRepositories());
    const members = await controller.getGroupMembers(id);
    res.json({ members });
  } else {
    res.status(400).send();
  }
});

router.post("/:id/members", async (req, res) => {
  try {
    const groupId = Number(req.params.id);
    const userIds = req.body.userIds;
    if (groupId && !isNaN(groupId) && userIds && userIds.length > 0) {
      const controller = new GroupController(getAppRepositories());
      const group = await controller.addGroupMembers(groupId, userIds);
      res.json({ group });
    } else {
      res.status(400).send();
    }
  } catch (error) {
    console.error(error);
    res.status(500).send();
  }
});

export default router;
