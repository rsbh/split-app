import express from "express";
import UserController from "../controllers/user.controller";
import { getAppRepositories } from "../repositories";

const router = express.Router();

router.get("/", async (req, res) => {
  const controller = new UserController(getAppRepositories());
  const users = await controller.getUsers();
  res.json({ users });
});

router.post("/", async (req, res) => {
  const controller = new UserController(getAppRepositories());
  await controller.createUser(req.body);
  res.status(201).send();
});

router.get("/:id", async (req, res) => {
  const id = Number(req.params.id);
  if (id && !isNaN(id)) {
    const controller = new UserController(getAppRepositories());
    const user = await controller.getUserById(id);
    res.json({ user });
  } else {
    res.status(400).send();
  }
});

export default router;
