import express from "express";
import userRouter from "./user.router";
import groupRouter from "./group.router";
import expenseRouter from "./expense.router";

const router = express.Router();

router.use("/users", userRouter);
router.use("/groups", groupRouter);
router.use("/expenses", expenseRouter);

export default router;
