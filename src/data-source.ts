import dbConfig from "./config/database";

import { DataSource } from "typeorm";

const AppDataSource = new DataSource(dbConfig);

export default AppDataSource;
