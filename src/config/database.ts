import { DataSourceOptions } from "typeorm";
import { User } from "../models/user";
import { Group } from "../models/group";
import { Expense } from "../models/expense";
import { Contribution } from "../models/contribution";

const config: DataSourceOptions = {
  type: "postgres",
  host: process.env.POSTGRES_HOST || "localhost",
  port: Number(process.env.POSTGRES_PORT) || 5432,
  username: process.env.POSTGRES_USER || "postgres",
  password: process.env.POSTGRES_PASSWORD || "postgres",
  database: process.env.POSTGRES_DB || "postgres",
  entities: [User, Group, Expense, Contribution],
  synchronize: true,
};

export default config;
