import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Contribution } from "./contribution";
import { Group } from "./group";

@Entity()
export class Expense {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column()
  amount!: number;

  @ManyToOne(() => Group, (group: Group) => group.expenses)
  @JoinColumn({ name: "groupId" })
  group!: Group;

  @Column({ nullable: true })
  groupId!: number;

  @OneToMany(
    () => Contribution,
    (contribution: Contribution) => contribution.expense,
    {
      cascade: true,
      eager: true,
    }
  )
  contributions!: Contribution[];

  @CreateDateColumn()
  created_at!: Date;

  @UpdateDateColumn()
  updated_at!: Date;
}
