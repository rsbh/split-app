import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Contribution } from "./contribution";
import { Group } from "./group";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column()
  email!: string;

  @Column()
  password!: string;

  @ManyToMany(() => Group, (group: Group) => group.members)
  groups!: Group[];

  @OneToMany(
    () => Contribution,
    (contribution: Contribution) => contribution.user
  )
  contributions!: Contribution[];

  @CreateDateColumn()
  created_at!: Date;

  @UpdateDateColumn()
  updated_at!: Date;
}
