import "reflect-metadata";

import App from "./app";
import AppDataSource from "./data-source";

const PORT = process.env.PORT || 8000;

AppDataSource.initialize()
  .then(() => {
    console.log("Data Source has been initialized!");
    const app = new App({ port: PORT });
    app.start();
  })
  .catch((err) => {
    console.error("Error during Data Source initialization", err);
  });
