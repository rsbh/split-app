import express, { Application } from "express";
import morgan from "morgan";

import apiRouter from "./routes";

interface AppConfig {
  port: number | string;
}

export default class App {
  public app: Application;
  private port: number | string;

  constructor(config: AppConfig) {
    this.app = express();
    this.port = config.port;
    this.setupMiddlewares();
    this.setupRoutes();
  }

  private setupMiddlewares(): void {
    this.app.use(express.json());
    this.app.use(morgan("tiny"));
  }

  private setupRoutes(): void {
    this.app.use("/api", apiRouter);
  }

  public async start(): Promise<void> {
    this.app.listen(this.port, () => {
      console.log(`Server listening on port ${this.port}`);
    });
  }
}
