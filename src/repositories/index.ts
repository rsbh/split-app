import { Repository } from "typeorm";
import AppDataSource from "../data-source";
import { Expense } from "../models/expense";
import { Group } from "../models/group";
import { User } from "../models/user";

export interface AppRepositories {
  group: Repository<Group>;
  user: Repository<User>;
  expense: Repository<Expense>;
}

export function getAppRepositories() {
  const group = AppDataSource.getRepository(Group);
  const user = AppDataSource.getRepository(User);
  const expense = AppDataSource.getRepository(Expense);

  return {
    group,
    user,
    expense,
  };
}
