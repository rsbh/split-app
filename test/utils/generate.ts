import { faker } from "@faker-js/faker";

export function generateUserData(overide = {}) {
  return {
    id: faker.random.numeric(),
    name: faker.name.firstName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
    createdAt: new Date(),
    updatedAt: new Date(),
    ...overide,
  };
}

export function generateUsersData(n = 1) {
  return Array.from(
    {
      length: n,
    },
    () => {
      return generateUserData();
    }
  );
}

export function generateUserPayload() {
  return {
    name: faker.name.firstName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
  };
}

export function generateGroupData(overide = {}) {
  return {
    id: faker.random.numeric(),
    name: faker.random.word(),
    createdAt: new Date(),
    updatedAt: new Date(),
    ...overide,
  };
}

export function generateGroupsData(n = 1) {
  return Array.from(
    {
      length: n,
    },
    () => {
      return generateGroupData();
    }
  );
}

export function generateGroupPayload() {
  return {
    name: faker.random.word(),
  };
}
